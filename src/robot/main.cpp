#include <ros/ros.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <common/machine_controller.hpp>

class Robot : public MachineController<assembly_control_ros::robot_state,
                                        assembly_control_ros::robot_input,
                                        assembly_control_ros::robot_command,
                                        assembly_control_ros::robot_output> {
public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::Rob_Wait),position_(State::ASS) {
    }

    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

        case State::Rob_Wait:        

        //Grasp
            if (inputs.rob_grasp) {
                inputs.rob_grasp = false;
                ROS_INFO("[Robot] Le robot va grasp");
                state_ = State::Rob_Grasp;
            }

        //Release
            if (inputs.rob_release) {
                inputs.rob_release = false;
                ROS_INFO("[Robot] Le robot va relacher la part");
                state_ = State::Rob_Release;
            }

        //Assemble Pt 1
            if (inputs.start_ass_pt1) {
                inputs.start_ass_pt1 = false;
                ROS_INFO("[Robot] Le robot va assembler la part 1");
                state_ = State::Rob_Ass_Part1;
            }

        //Assemble Pt 2
            if (inputs.start_ass_pt2) {
                inputs.start_ass_pt2 = false;
                ROS_INFO("[Robot] Le robot va assembler la part 2");
                state_ = State::Rob_Ass_Part2;
            }

        //Assemble Pt 3
            if (inputs.start_ass_pt3) {
                inputs.start_ass_pt3 = false;
                ROS_INFO("[Robot] Le robot va assembler la part 3");
                state_ = State::Rob_Ass_Part3;
            }

        //Go Left
            if (inputs.go_left){
                inputs.go_left = false;
                ROS_INFO("[Robot] Le robot va se deplacer a gauche");
                state_ = State::RobMoveToLeftPlace;

            }    

        //Go Right
            if (inputs.go_right){
                ROS_INFO("[Robot] Le robot va se deplacer a droite");
                state_ = State::RobMoveToRightPlace;

            }    
            break;

         case State::RobMoveToLeftPlace: 
            commands.move_left=true;

            if (position_==State::SC && getState().at_assembly_station ) {
                commands.move_left = false;
                outputs.rob_at_AS=true;
                ROS_INFO("[Robot] Le robot est a la station d'assemblage");
                position_=State::ASS;
                state_ = State::RobHasMovedToLeftPlace;
                
            }

            if( position_==State::EC && getState().at_supply_conveyor){
                commands.move_left = false;
                outputs.rob_at_SC=true;
                ROS_INFO("[Robot] Le robot est au Supply Conveyor");
                position_=State::SC;
                state_ = State::RobHasMovedToLeftPlace;
              

            }
            sendOuputs(outputs);
            break;   

        case State::RobHasMovedToLeftPlace: 
            ROS_INFO("[Robot] Le robot a fini de se deplacer vers la gauche");
            state_ = State::Rob_Wait;
             break;

         case State::RobMoveToRightPlace: 
                commands.move_right = true;
         
            if (position_==State::SC && getState().at_evacuation_conveyor) {
                commands.move_right = false;
                outputs.rob_at_EC=true;
                inputs.go_right = false;
                ROS_INFO("[Robot] Le robot est a l'EC'");
                position_=State::EC;
                state_ = State::RobHasMovedToLeftPlace;
                
            }

            if( position_==State::ASS && getState().at_supply_conveyor){
                commands.move_right = false;
                outputs.rob_at_SC=true;
                inputs.go_right = false;
                ROS_INFO("[Robot] Le robot est au Supply Conveyor");
                position_=State::SC;
                state_ = State::RobHasMovedToRightPlace;
                

            }
             sendOuputs(outputs);
            break;

        case State::RobHasMovedToRightPlace: 
            ROS_INFO("[Robot] Le robot a fini de se deplacer vers la droite");
            state_ = State::Rob_Wait;
             break;

        case State::Rob_Grasp:
        commands.grasp=true;
            if (getState().part_grasped) {
                commands.grasp = false;
                outputs.rob_part_grasped=true;
                sendOuputs(outputs);
                ROS_INFO("[Robot] La piece est attrapee par le robot");
                state_ = State::Rob_Wait;
            }
            break;

        case State::Rob_Release:
            commands.release=true;
            if (getState().part_released) {
                commands.release = false;
                outputs.release_done=true;
                sendOuputs(outputs);
                ROS_INFO("[Robot] La piece est relachee par le robot");
                state_ = State::Rob_Wait;
            }
            break;
        
        case State::Rob_Ass_Part1:
        commands.assemble_part1=true;

            if (getState().part1_assembled) {
                commands.assemble_part1 = false;
                outputs.rob_ass_part1_done=true;
                sendOuputs(outputs);
                ROS_INFO("[Robot] La piece 1 est assemblee");
                state_ = State::Rob_Wait;
            }
            break;

        case State::Rob_Ass_Part2:
        commands.assemble_part2=true;

            if (getState().part2_assembled) {
                commands.assemble_part2 = false;
                outputs.rob_ass_part2_done=true;
                sendOuputs(outputs);
                ROS_INFO("[Robot] La piece 2 est assemblee");
                state_ = State::Rob_Wait;
            }
            break;
            
        case State::Rob_Ass_Part3:
        commands.assemble_part3=true;

            if (getState().part3_assembled) {
                commands.assemble_part3 = false;
                outputs.rob_ass_part3_done=true;
                sendOuputs(outputs);
                ROS_INFO("[Robot] La piece 3 est assemblee");
                state_ = State::Rob_Wait;
            }
            break;
        
        
        
        }

        sendCommands(commands);
    }

private:
    enum class State { Rob_Wait, Rob_Grasp, Rob_Release, Rob_Ass_Part1, 
                        Rob_Ass_Part2, Rob_Ass_Part3, RobMoveToLeftPlace,RobMoveToRightPlace,
                        RobHasMovedToLeftPlace,RobHasMovedToRightPlace,ASS,SC,EC};

    State state_;
    State position_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}