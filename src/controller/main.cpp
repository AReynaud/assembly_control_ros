#include <ros/ros.h>
#include <string>

#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>

#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <common/machine_controller.hpp>

//template < typename InputMsgT,typename OutputMsgT>

class Controller {

public:
    Controller(ros::NodeHandle node)
        : node_(node),
          state_(State::WaitPart),
          stock1_(Stock::Empty),
          stock2_(Stock::Empty),
          stock3_(Stock::Empty) {

        // Ici on permet au controleur de publier sur les topics propres
        // à chaque input reçus par les machines
        input_robot_publisher               = node.advertise<assembly_control_ros::robot_input>("/robot/input",10);
        input_assembly_station_publisher    = node.advertise<assembly_control_ros::assembly_station_input>("/assembly_station/input", 10);
        input_camera_publisher              = node.advertise<assembly_control_ros::camera_input>("/camera/input",10);
        input_supply_conveyor_publisher     = node.advertise<assembly_control_ros::supply_conveyor_input>("/supply_conveyor/input", 10);
        input_evacuation_conveyor_publisher = node.advertise<assembly_control_ros::evacuation_conveyor_input>("/evacuation_conveyor/input", 10);

        // Ici on permet au controleur de se connecter au topic propre
        // à chaque output envoyé par les machines
        output_robot_subscriber               = node.subscribe("/robot/output", 10, &Controller::outputCallbackRobot, this);
        output_assembly_station_subscriber    = node.subscribe("/assembly_station/output", 10,&Controller::outputCallbackAssemblyStation, this);
        output_camera_subscriber              = node.subscribe("/camera/output", 10, &Controller::outputCallbackCamera, this);
        output_supply_conveyor_subscriber     = node.subscribe("/supply_conveyor/output", 10,&Controller::outputCallbackSupplyConveyor, this);
        output_evacuation_conveyor_subscriber = node.subscribe("/evacuation_conveyor/output", 10,&Controller::outputCallbackEvacuationConveyor, this);
    } //end constructor

    void process() {

        assembly_control_ros::robot_output outputs_robot                             = getOutputsR();
        assembly_control_ros::supply_conveyor_output outputs_supply_conveyor         = getOutputsSC();
        assembly_control_ros::camera_output outputs_camera                           = getOutputsC();
        assembly_control_ros::assembly_station_output outputs_assembly_station       = getOutputsAS();
        assembly_control_ros::evacuation_conveyor_output outputs_evacuation_conveyor = getOutputsEC();

        assembly_control_ros::camera_input inputs_camera;
        assembly_control_ros::robot_input inputs_robot;
        assembly_control_ros::supply_conveyor_input inputs_supply_conveyor;
        assembly_control_ros::evacuation_conveyor_input inputs_evacuation_conveyor;
        assembly_control_ros::assembly_station_input inputs_assembly_station;

        switch (state_) {
        case State::WaitPart:
            if (outputs_supply_conveyor.part_available) {
                outputs_supply_conveyor.part_available = false;
                inputs_camera.start_recognition = true;
                inputs_robot.go_right = true;
                sendInputs("camera", inputs_camera);
                sendInputs("robot", inputs_robot);

                ROS_INFO("[Controller] Part disponible, go at SC");
                state_ = State::WaitForAtSC;
            }
            break;
        
        case State::WaitForAtSC:
            
            if  ((outputs_robot.rob_at_SC) && (outputs_camera.part_analyzed)) {
                outputs_robot.rob_at_SC = false;
                outputs_camera.part_analyzed = false;
                inputs_robot.rob_grasp = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] At SS, pret pour Grasp");
                state_ = State::WaitForGrasp;
            }
            break;
        case State::WaitForGrasp:
            if (outputs_robot.rob_part_grasped) {
                outputs_robot.rob_part_grasped = false;
                inputs_supply_conveyor.restart = true;
                sendInputs("supply_conveyor", inputs_supply_conveyor);
                ROS_INFO("[Controller] Part Grasped, maintenant -> Decision");
                state_ = State::WaitForDecision;
            }
            break;
        case State::WaitForDecision:
            if ((outputs_camera.part1) && (stock1_ == Stock::Full)) {
                outputs_camera.part1 = false;
                inputs_robot.go_right = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] Part 1 deja occupee, go EC");
                state_ = State::WaitForAtEC;
            }
            if ((outputs_camera.part1) && (stock1_ == Stock::Empty)) {
                inputs_robot.go_left = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] Part 1 non occupee, go ASS");
                state_ = State::WaitForAtAss;
            }

            if ((outputs_camera.part2) && (stock2_ == Stock::Full)) {
                outputs_camera.part2 = false;
                inputs_robot.go_right = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] Part 2 deja occupee, go EC");
                state_ = State::WaitForAtEC;
            }
            if ((outputs_camera.part2) && (stock2_ == Stock::Empty)) {
                inputs_robot.go_left = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] Part 2 non occupee, go ASS");
                state_ = State::WaitForAtAss;
            }

            if ((outputs_camera.part3) && (stock3_ == Stock::Full)) {
                outputs_camera.part3 = false;
                inputs_robot.go_right = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] Part 3 deja occupee, go EC");
                state_ = State::WaitForAtEC;
            }
            if ((outputs_camera.part3) && (stock3_ == Stock::Empty)) {
                inputs_robot.go_left = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] Part 3 non occupee, go ASS");
                state_ = State::WaitForAtAss;
            }
            break;

        case State::WaitForAtEC:
            if (outputs_robot.rob_at_EC) {
                outputs_robot.rob_at_EC = false;
                inputs_evacuation_conveyor.stop_EC = true;
                sendInputs("evacuation_conveyor", inputs_evacuation_conveyor);
                ROS_INFO("[Controller] Rob est a EC, en attente de l'arret d'EC");
                state_ = State::WaitForECStopped;
            }

            break;

        case State::WaitForECStopped:
            if (outputs_evacuation_conveyor.stopped_EC) {
                outputs_evacuation_conveyor.stopped_EC = false;
                inputs_robot.rob_release = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] l'EC est a l'arret, le robot peut relacher la piece");
                state_ = State::WaitForRelease;
            }
            break;

        case State::WaitForRelease:
            if (outputs_robot.release_done && outputs_supply_conveyor.part_available) {
                outputs_robot.release_done = false;
                outputs_supply_conveyor.part_available = false;
                inputs_evacuation_conveyor.restart_EC = true;
                inputs_robot.go_left = true;
                inputs_camera.start_recognition = true;
                sendInputs("evacuation_conveyor", inputs_evacuation_conveyor);
                sendInputs("robot", inputs_robot);
                sendInputs("camera", inputs_camera);
                ROS_INFO("[Controller] La piece a ete relachee, l'EC peut redemarrer et go SC");
                state_ = State::WaitECtoSC;
            }
            break;

        case State::WaitECtoSC:
            if (outputs_robot.rob_at_SC && outputs_camera.part_analyzed) {
                outputs_robot.rob_at_SC = false;
                outputs_camera.part_analyzed=false;
                inputs_robot.rob_grasp = true;
                sendInputs("robot", inputs_robot);
                ROS_INFO("[Controller] le robot est a SC, pret a grasp");
                state_ = State::WaitForGrasp;
            }
            break;

        case State::WaitSCtoAS: 
            if (outputs_robot.rob_at_AS) {
                outputs_robot.rob_at_AS = false;
                ROS_INFO("[Controller] le robot est a AS, retour au statut initial");
                state_ = State::WaitPart;
            }
            break;

        case State::WaitForAtAss:
            if (outputs_robot.rob_at_AS) {
                outputs_robot.rob_at_AS = false;

                if (outputs_camera.part1) {
                    outputs_camera.part1 = false;
                    inputs_robot.start_ass_pt1 = true;
                    sendInputs("robot", inputs_robot);

                    ROS_INFO("[Controller] le robot va assembler la partie 1");
                    state_ = State::WaitForAss1Done;
                }

                if (outputs_camera.part2) {
                    outputs_camera.part2 = false;
                    inputs_robot.start_ass_pt2 = true;
                    sendInputs("robot", inputs_robot);

                    ROS_INFO("[Controller] le robot va assembler la partie 2");
                    state_ = State::WaitForAss2Done;
                }

                if (outputs_camera.part3) {
                    outputs_camera.part3 = false;
                    inputs_robot.start_ass_pt3 = true;
                    sendInputs("robot", inputs_robot);

                    ROS_INFO("[Controller] le robot va assembler la partie 3");
                    state_ = State::WaitForAss3Done;
                }
            }
            break;

        case State::WaitForAss1Done:
            if (outputs_robot.rob_ass_part1_done) {
                outputs_robot.rob_ass_part1_done = false;
                ROS_INFO("[Controller] La piece 1 a ete assemblee, retour du robot a l'etat initial");

                stock1_ = Stock::Full;

                if (stock1_ == Stock::Full && stock2_ == Stock::Full && stock3_ == Stock::Full) {
                    stock1_ = Stock::Empty;
                    stock2_ = Stock::Empty;
                    stock3_ = Stock::Empty;
                    inputs_assembly_station.all_parts_assembled = true;
                    sendInputs("assembly_station", inputs_assembly_station);
                }
                state_ = State::WaitPart;
            }
            break;

        case State::WaitForAss2Done:
            if (outputs_robot.rob_ass_part2_done) {
                outputs_robot.rob_ass_part2_done = false;
                ROS_INFO("[Controller] La piece 2 a ete assemblee, retour du robot a l'etat initial");

                stock2_ = Stock::Full;

                if (stock1_ == Stock::Full && stock2_ == Stock::Full && stock3_ == Stock::Full) {
                    stock1_ = Stock::Empty;
                    stock2_ = Stock::Empty;
                    stock3_ = Stock::Empty;
                    inputs_assembly_station.all_parts_assembled = true;
                    sendInputs("assembly_station", inputs_assembly_station);
                }
                state_ = State::WaitPart;
            }
            break;

        case State::WaitForAss3Done:
            if (outputs_robot.rob_ass_part3_done) {
                outputs_robot.rob_ass_part3_done = false;
                ROS_INFO("[Controller] La piece 3 a ete assemblee, retour du robot a l'etat initial");

                stock3_ = Stock::Full;

                if (stock1_ == Stock::Full && stock2_ == Stock::Full && stock3_ == Stock::Full) {
                    stock1_ = Stock::Empty;
                    stock2_ = Stock::Empty;
                    stock3_ = Stock::Empty;
                    inputs_assembly_station.all_parts_assembled = true;
                    sendInputs("assembly_station", inputs_assembly_station);
                }
                state_ = State::WaitPart;
            }
            break;

        } //end switch

    } // end function process

protected:

    void sendInputs(std::string machine, auto input_message) {

        if (machine == "robot")
            input_robot_publisher.publish(input_message);

        if (machine == "assembly_station")
            input_assembly_station_publisher.publish(input_message);

        if (machine == "camera")
            input_camera_publisher.publish(input_message);

        if (machine == "supply_conveyor")
            input_supply_conveyor_publisher.publish(input_message);

        if (machine == "evacuation_conveyor")
            input_evacuation_conveyor_publisher.publish(input_message);
    };

    
    assembly_control_ros::robot_output getOutputsR() {
        return output_message_robot;
    };

    assembly_control_ros::supply_conveyor_output getOutputsSC() {
        return output_message_supply_conveyor;
    };

    assembly_control_ros::camera_output getOutputsC() {
        return output_message_camera;
    };

    assembly_control_ros::evacuation_conveyor_output getOutputsEC() {
        return output_message_evacuation_conveyor;
    };

    assembly_control_ros::assembly_station_output getOutputsAS() {
        return output_message_assembly_station;
    };

    /*
    
    auto& getOutputs(std::string machine) { 

          if(machine=="robot")
            return output_message_robot;
        

            if(machine=="supply_conveyor") 
            return output_message_supply_conveyor;
            

            if(machine=="camera") 
            return output_message_camera;
            

            if(machine=="evacuation_conveyor")  
            return output_message_evacuation_conveyor;
            

            if(machine=="assembly_station") 
            return output_message_assembly_station;
            
            --------> pas bon car le auto& veut un return const
            */

private:
    void outputCallbackRobot(
        const typename assembly_control_ros::robot_output::ConstPtr& message) {
        output_message_robot = *message;
    }

    void outputCallbackSupplyConveyor(
        const typename assembly_control_ros::supply_conveyor_output::ConstPtr& message) {
        output_message_supply_conveyor = *message;
    }

    void outputCallbackCamera(
        const typename assembly_control_ros::camera_output ::ConstPtr& message) {
        output_message_camera = *message;
    }

    void outputCallbackEvacuationConveyor(
        const typename assembly_control_ros::evacuation_conveyor_output::ConstPtr& message) {
        output_message_evacuation_conveyor = *message;
    }

    void outputCallbackAssemblyStation(
        const typename assembly_control_ros::assembly_station_output::ConstPtr& message) {
        output_message_assembly_station = *message;
    }

    ros::NodeHandle node_;
    ros::Publisher input_robot_publisher;
    ros::Publisher input_assembly_station_publisher;
    ros::Publisher input_camera_publisher;
    ros::Publisher input_supply_conveyor_publisher;
    ros::Publisher input_evacuation_conveyor_publisher;

    ros::Subscriber output_robot_subscriber;
    ros::Subscriber output_assembly_station_subscriber;
    ros::Subscriber output_camera_subscriber;
    ros::Subscriber output_supply_conveyor_subscriber;
    ros::Subscriber output_evacuation_conveyor_subscriber;

    assembly_control_ros::robot_output output_message_robot;
    assembly_control_ros::supply_conveyor_output output_message_supply_conveyor;
    assembly_control_ros::camera_output output_message_camera;
    assembly_control_ros::evacuation_conveyor_output output_message_evacuation_conveyor;
    assembly_control_ros::assembly_station_output output_message_assembly_station;


    enum class State {
        WaitPart,
        WaitForAtSC,
        WaitForGrasp,
        WaitForDecision,
        WaitForAtEC,
        WaitForECStopped,
        WaitForRelease,
        WaitECtoSC,
        WaitSCtoAS,
        WaitForAtAss,
        WaitForAss1Done,
        WaitForAss2Done,
        WaitForAss3Done
    };
    enum class Stock {
        Full,
        Empty        
    };

    State state_;
    Stock stock1_;
    Stock stock2_;
    Stock stock3_;
};


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controller");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Controller controller(node);

    while (ros::ok()) {
        controller.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}