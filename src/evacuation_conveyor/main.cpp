#include <ros/ros.h>

#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <common/machine_controller.hpp>

class Evacuation_conveyor : public MachineController<assembly_control_ros::evacuation_conveyor_state,
                                        assembly_control_ros::evacuation_conveyor_input,
                                        assembly_control_ros::evacuation_conveyor_command,
                                        assembly_control_ros::evacuation_conveyor_output> {
public:
    Evacuation_conveyor(ros::NodeHandle node)
        : MachineController(node, "evacuation_conveyor"), state_(State::EC_ON) {
    }

    virtual void process() override {
        assembly_control_ros::evacuation_conveyor_command commands;
        assembly_control_ros::evacuation_conveyor_output outputs;

        auto& inputs = getInputs();

        switch (state_) {
        case State::EC_ON :
        commands.on=true;
            if (inputs.stop_EC) {
                inputs.stop_EC = false;

                ROS_INFO("[Evacuation Conveyor] Arret de l'EC");
                outputs.stopped_EC=true;
                sendOuputs(outputs);
                state_ = State::EC_OFF;
            }
            break;
        case State::EC_OFF:
            if (inputs.restart_EC) {
                inputs.restart_EC=false;

                ROS_INFO("[Evacuation Conveyor] Redemarrage de l'EC");
                state_ = State::EC_ON;
            }
            break;
        
        }

        sendCommands(commands);
    }

private:
    enum class State { EC_ON, EC_OFF };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "evacuation_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Evacuation_conveyor evacuation_conveyor(node);

    while (ros::ok()) {
        evacuation_conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}