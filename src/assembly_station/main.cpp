#include <ros/ros.h>

#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <common/machine_controller.hpp>

class AssemblyStation : public MachineController<assembly_control_ros::assembly_station_state,
                                        assembly_control_ros::assembly_station_input,
                                        assembly_control_ros::assembly_station_command,
                                        assembly_control_ros::assembly_station_output> {
public:
    AssemblyStation(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::AS_Wait) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

        case State::AS_Wait:
        commands.check = true;
            if (inputs.all_parts_assembled) {
                inputs.all_parts_assembled = false;        
                ROS_INFO("[Assembly Station] La verification de l'assemblage va démarrer");
                state_ = State::AS_Check;
            }
            break;

        case State::AS_Check:
            if (getState().valid) {
                commands.check = false;
                ROS_INFO("[Assembly Station] Assemblage vérifié et validé");
                state_ = State::AS_Evacuating;
            }
            break;

        case State::AS_Evacuating:
            if (getState().evacuated) {               
                ROS_INFO("[Assembly Station] Assemblage évacué");
                state_ = State::AS_Wait;
            }
            break;
            

        }

        sendCommands(commands);
    }

private:
    enum class State { AS_Wait, AS_Check, AS_Evacuating };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation assembly_station(node);

    while (ros::ok()) {
        assembly_station.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}